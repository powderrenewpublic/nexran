/*
 * Copyright (c) 2022-2023 The University of Utah and the Flux Group.
 */

#ifndef __RESTSERVER_XAPP__H__
#define __RESTSERVER_XAPP__H__

#include <string>

#include <pistache/endpoint.h>
#include <pistache/router.h>

#include "rapidjson/prettywriter.h"

namespace nexran {

class App;

class RestServerXApp {
 public:

    RestServerXApp()
	: running(false), app(NULL) {};
    virtual void init(App *app_);
    virtual void start();
    virtual void stop();
    virtual ~RestServerXApp() { stop(); };

 private:
    void setupRoutes();
    void buildConfigJson();

    void getConfig(const Pistache::Rest::Request &request,
		   Pistache::Http::ResponseWriter response);
    void getAlive(const Pistache::Rest::Request &request,
		  Pistache::Http::ResponseWriter response);
    void getReady(const Pistache::Rest::Request &request,
		  Pistache::Http::ResponseWriter response);

    bool running;
    App *app;
    Pistache::Http::Endpoint endpoint;
    Pistache::Rest::Router router;
    std::string cached_json_config;
};

}

#endif /* __RESTSERVER_XAPP__H__ */
