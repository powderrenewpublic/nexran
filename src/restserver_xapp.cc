/*
 * Copyright (c) 2022-2023 The University of Utah and the Flux Group.
 */

#include <unistd.h>
#include <fstream>
#include <sstream>

#include "mdclog/mdclog.h"
#include "pistache/string_logger.h"
#include "pistache/tcp.h"

#include "nexran.h"
#include "version.h"
#include "restserver_xapp.h"

namespace nexran {

#define VERSION_PREFIX "/ric/v1/"

void RestServerXApp::setupRoutes()
{
    Pistache::Rest::Routes::Get(
	router,VERSION_PREFIX "/config",
	Pistache::Rest::Routes::bind(&RestServerXApp::getConfig,this));
    Pistache::Rest::Routes::Get(
	router,VERSION_PREFIX "/health/alive",
	Pistache::Rest::Routes::bind(&RestServerXApp::getAlive,this));
    Pistache::Rest::Routes::Get(
	router,VERSION_PREFIX "/health/ready",
	Pistache::Rest::Routes::bind(&RestServerXApp::getReady,this));
}

void RestServerXApp::init(App *app_)
{
    Pistache::Port port(
        app_->config[Config::ItemName::XAPP_API_PORT]->i);
    Pistache::Address addr(
        std::string(app_->config[Config::ItemName::XAPP_API_HOST]->s),
	port);

    app = app_;

    buildConfigJson();
    setupRoutes();
    auto options = Pistache::Http::Endpoint::options().logger(
	std::make_shared<Pistache::Log::StringToStreamLogger>(
	    Pistache::Log::Level::DEBUG));
    options.flags(Pistache::Tcp::Options::ReuseAddr);
    endpoint.init(options);
    endpoint.setHandler(router.handler());
    endpoint.bind(addr);

    mdclog_write(MDCLOG_DEBUG,"initialized xApp internal interface (%s:%d",
		 app_->config[Config::ItemName::XAPP_API_HOST]->s,
		 app_->config[Config::ItemName::XAPP_API_PORT]->i);
}

void RestServerXApp::start()
{
    if (running)
	return;

    endpoint.serveThreaded();
    running = true;

    mdclog_write(MDCLOG_DEBUG,"started xApp internal interface");
}

void RestServerXApp::buildConfigJson()
{
    rapidjson::StringBuffer sb;
    rapidjson::Writer<rapidjson::StringBuffer> writer(sb);

    /* Read our xApp's config file. */
    std::string path = "/opt/ric/config/config-file.json";
    if (!access(path.c_str(),R_OK) == 0) {
	path = "/nexran/etc/config-file.json";
	if (!access(path.c_str(),R_OK) == 0) {
	    path = "";
	    mdclog_write(MDCLOG_ERR,"cannot find config-file.json in /opt/ric/config/ nor /nexran/etc");
	}
    }
    std::string config_data;
    if (path.length() > 0) {
	std::ifstream cfin(path,std::ios::in | std::ios::binary);
	if (cfin) {
	    std::stringstream ss;
	    ss << cfin.rdbuf();
	    config_data = ss.str();
	}
	else
	    mdclog_write(MDCLOG_ERR,"failed to read %s", path.c_str());
    }

    writer.StartArray();
    writer.StartObject();
    writer.String("config");
    writer.String(config_data.c_str());
    writer.String("metadata");
    writer.StartObject();
    writer.String("configType");
    writer.String("json");
    writer.String("xappName");
    writer.String("nexran");
    writer.EndObject();
    writer.EndObject();
    writer.EndArray();

    cached_json_config = sb.GetString();
}

void RestServerXApp::getConfig(
    const Pistache::Rest::Request &request,
    Pistache::Http::ResponseWriter response)
{
    response.send(Pistache::Http::Code::Ok,cached_json_config);
}

void RestServerXApp::getAlive(
    const Pistache::Rest::Request &request,
    Pistache::Http::ResponseWriter response)
{
    response.send(Pistache::Http::Code::Ok);
}

void RestServerXApp::getReady(
    const Pistache::Rest::Request &request,
    Pistache::Http::ResponseWriter response)
{
    if (app->is_rmr_running())
	response.send(Pistache::Http::Code::Ok);
    else
	response.send(Pistache::Http::Code::Service_Unavailable);
}

void RestServerXApp::stop()
{
    if (!running)
	return;

    endpoint.shutdown();
    running = false;
}

}
